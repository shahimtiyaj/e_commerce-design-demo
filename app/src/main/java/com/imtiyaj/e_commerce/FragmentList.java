package com.imtiyaj.e_commerce;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

public class FragmentList extends Fragment {
    public FragmentList() {
        // Required empty public constructor
    }

    private ListView myliListView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.listview, container, false);

        Spinner spin = (Spinner) view.findViewById(R.id.spinner);

        myliListView = (ListView) view.findViewById(R.id.list_shopping);


        myliListView.setAdapter(new CustomAdapter(getContext()));


        return view;
    }

    public static class ViewHolder {
        public TextView titles;
        public TextView Description;
        public ImageView images;
    }

    class SingleRow {
        String titles;
        String Description;
        String images;

        SingleRow(String titles, String description, String images) {
            this.titles = titles;
            this.Description = description;
            this.images = images;
        }

    }

    public class CustomAdapter extends BaseAdapter {

        Context contexT;

        LayoutInflater inflater;
        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options;
        String[] texts = {

                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",

                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",

                "",


        };
        String[] images = {

                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",

                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",

                "",

        };

        public CustomAdapter(Context context) {
            contexT = context;

            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            Resources res = context.getResources();

            // String[] titles = res.getStringArray(R.array.titles);
            // String[] description = res.getStringArray(R.array.descriptions);

            imageLoader.init(ImageLoaderConfiguration.createDefault(context));
            options = new DisplayImageOptions.Builder()
                    .showImageForEmptyUri(R.drawable.perfume)
                    .showImageOnFail(R.drawable.perfume)
                    .resetViewBeforeLoading(true).cacheOnDisc(true)
                    .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
                    .bitmapConfig(Bitmap.Config.ALPHA_8)
                    .considerExifParams(true)
                    .displayer(new FadeInBitmapDisplayer(50)).build();

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return images.length;
        }

        @Override
        public Object getItem(int pos) {
            // TODO Auto-generated method stub
            return pos;
        }

        @Override
        public long getItemId(int arg0) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            inflater = (LayoutInflater) contexT
                    .getSystemService(contexT.LAYOUT_INFLATER_SERVICE);
            View v;
            ViewHolder holder = null;
            if (convertView == null) {
                v = inflater.inflate(R.layout.list_item, parent, false);
                holder = new ViewHolder();
                v.setTag(holder);
            } else {
                v = convertView;
                holder = (ViewHolder) v.getTag();
            }
            convertView = inflater.inflate(R.layout.list_item, null);
            ImageView IV = (ImageView) convertView
                    .findViewById(R.id.imageView1);
            // IV.setBackground(images[pos]);
            imageLoader.displayImage(images[pos], IV, options);


            return convertView;
        }

    }

}

